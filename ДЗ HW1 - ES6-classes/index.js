// Теоретичне питання
// 1) Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// 2) Для чого потрібно викликати super() у конструкторі класу-нащадка?

// 1)Прототипне успадкування – це можливість мови, яка допомагає у цьому.
// Прототип дає нам трохи магії. Коли ми хочемо прочитати властивість із object, а вона відсутня, JavaScript автоматично бере її з прототипу.
// Властивість [[Prototype]] є внутрішньою та прихованою, але є багато способів задати його.

// 2) У конструкторі ключове слово super() використовується як функція, що викликає батьківський конструктор. Її необхідно викликати до першого звернення до ключового слова цього в тілі конструктора. Ключове слово super також можна використовувати для виклику функцій батьківського об'єкта.

"use strict"

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    set name(value) {
        this._name = value;
    }
    get name() {
        return this._name;
    }

    set age(value) {
        this._age = value;
    }
    get age() {
        return this._age;
    }

    set salary(value) {
        this._salary = value;
    }
    get salary() {
        return this._salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    set salary(value) {
        this._salary = value;
    }
    get salary() {
        return this._salary * 3;
    }
}

const employee1 = new Employee('Daniil', 23, 14500);
console.log(employee1);

const programmer1 = new Programmer('Yevgen', 35, 32000, ['js', 'c++']);
console.log(programmer1);

const programmer2 = new Programmer('Maria', 24, 14500, ['js', 'c++', 'java']);
console.log(programmer2);

console.log(programmer1.salary)
console.log(programmer2.salary)