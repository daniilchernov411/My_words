const buttonIp = document.querySelector("button");
const ulFind = document.createElement("ul");
const urlIp = "https://api.ipify.org/?format=json";
const urlInfo = "http://ip-api.com/";

buttonIp.addEventListener('click', getIpInfo);

async function customGet(url, query = "") {
    const response = await fetch(`${url}${query}`);
    const data = await response.json();
    return data;
}

async function getIpInfo() {
    const dataIP = await customGet(urlIp);
    const dataInfo = await customGet(urlInfo, `json/${dataIP.ip}?
    fields=continent,country,regionName,city,district`);
    for (let essence in dataInfo) {
        createList(essence, dataInfo[essence]);
    }
    document.body.append(ulFind);

}

async function createList(essence, info) {
    const li = document.createElement("li");
    li.innerHTML = `${essence}: ${info}`;
    ulFind.append(li);
}
