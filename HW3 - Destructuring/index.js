// Теоретичне питання

// Поясніть своїми словами, як ви розумієте, що таке деструктуризація і навіщо вона потрібна

// Деструктуризация – это особый синтаксис присваивания, при котором можно присвоить массив или объект сразу нескольким переменным, разбив его на части.
// Вона потрібна
// 1. С ее помощью можно не только получать данные в переменные, но и менять порядок элементов.
// 2. Cокращает синтаксис.
// 3.В паре с оператором spread, можно обратно соединять в структуры, но те элементы, которые нужны и в том порядке, в котором нужно.
// 4.Деструктуризацию можно использовать в написании параметров функций, чтобы объект, массив, когда будет передаваться в качестве аргументов в функцию, сразу раскладывался и был доступ к определенным данным через локальные переменные, которые автоматически создадутся благодаря деструктуризации.


// Task 1

console.group('Task1');

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

let lastName = clients1.filter(client => clients2.indexOf(client) == -1)
let alllastName = [...lastName, ...clients2];

console.log(alllastName);
console.groupEnd();

/*-----------------------------------------------------------------*/

// Task 2

console.group('Task2');

const characters = [
    {
        name: "Елена",
        lastName: "Гилберт",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Кэролайн",
        lastName: "Форбс",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Аларик",
        lastName: "Зальцман",
        age: 31,
        gender: "man",
        status: "human"
    },
    {
        name: "Дэймон",
        lastName: "Сальваторе",
        age: 156,
        gender: "man",
        status: "vampire"
    },
    {
        name: "Ребекка",
        lastName: "Майклсон",
        age: 1089,
        gender: "woman",
        status: "vempire"
    },
    {
        name: "Клаус",
        lastName: "Майклсон",
        age: 1093,
        gender: "man",
        status: "vampire"
    }
];

const showShortInfo = ({ name, lastName, age }) => {
    const personsObj = {
        name,
        lastName,
        age,
    }
    return personsObj;
}

const charactersShortInfo = characters.map(persons => showShortInfo(persons));

console.log(charactersShortInfo);
console.groupEnd();

/*-----------------------------------------------------------------*/

// Task 3

console.group('Task3');

const user1 = {
    name: "John",
    years: 30
};

const { name = "ім'я", years = "вік", isAdmin = false } = user1;

console.log(name);
console.log(years);
console.log(isAdmin);

console.groupEnd();

/*-----------------------------------------------------------------*/

// Task 4

console.group('Task4');

const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
        lat: 38.869422,
        lng: 139.876632
    }
}

const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
}

const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto',
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
}

const fullProfile = { ...satoshi2018, ...satoshi2019, ...satoshi2020 };

console.log(fullProfile);
console.groupEnd();

/*-----------------------------------------------------------------*/

// Task 5

console.group('Task5');

const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
}, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
}, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
}

const ShowBooks = [...books, bookToAdd];

console.log(ShowBooks);
console.groupEnd();

/*-----------------------------------------------------------------*/

// Task 6

console.group('Task6');

const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}

const newEmployee = { ...employee, age: 25, salary: 4500 };

console.log(newEmployee);
console.groupEnd();

/*-----------------------------------------------------------------*/

// Task 7

console.group('Task7');

const array = ['value', () => 'showValue'];

const [value, showValue] = array; // Допишіть код тут

alert(value); // має бути виведено 'value'
alert(showValue());  // має бути виведено 'showValue'

console.log(value);
console.log(showValue());
console.groupEnd();