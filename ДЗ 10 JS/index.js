'use strict';

let tabs = document.querySelectorAll('.tabs li');

tabs.forEach(function(tabs, index){
    tabs.addEventListener('click', function(){
        let currentTabsData = document.querySelector('.active-text[data-get-content="' + this.dataset.getTrigger + '"]');

        document.querySelector('.tabs-content li.is-open').classList.remove('is-open');
        document.querySelector('.tabs li.active').classList.remove('active');
        currentTabsData.classList.add('is-open');
        this.classList.add('active');
    });
});





