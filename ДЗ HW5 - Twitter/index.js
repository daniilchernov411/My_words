"use strict"

const getuser = "https://ajax.test-danit.com/api/json/users";
const geturl = "https://ajax.test-danit.com/api/json/posts";
const cardsContainer = document.querySelector(".container")


class Card {
    constructor (name, email, title, body, id) {
        this.name = name;
        this.email = email;
        this.title = title;
        this.body = body;
        this.id = id;
        this.divCard = document.createElement('div')
        this.deleteBtn = document.createElement('button')
    }

    render(selector) {
        this.divCard.className = 'card';
        this.deleteBtn.className = 'card-delete';
        this.deleteBtn.innerText = 'DELETE'
        document.querySelector(selector).append(this.divCard)
        this.divCard.insertAdjacentHTML('beforeend',
            `<div class="user">
        <span>${this.id})</span>
        <span>Name: ${this.name}</span>
        <a href="mailto:${this.email}">Email: ${this.email}</a>
            <h4>${this.title}</h4>
            <p>${this.body}</p> 
            </div>
        `)
        this.divCard.append(this.deleteBtn);
        this.Carddelete();
    }
    Carddelete() {
        this.deleteBtn.addEventListener('click', (e) => {
            divCarddeleteOnServer(this.id, this.divCard)
        })
    }
}


const getUsersCards = () => {
    const users = fetch(getuser).then(response => response.json());
    const cards = fetch(geturl).then(response => response.json());

    Promise.allSettled([users, cards]).then((results) => {
        console.log(results);
        let usersArray = results[0].value;
        let urlsArray = results[1].value;

        urlsArray.forEach(el => {
            usersArray.forEach(({ id, name, email }) => {
                if (el.userId === id) {
                    el.name = name;
                    el.email = email;
                }
            })
        })
        return urlsArray
    })
        .then((usersAndurlsArray) => {
            usersAndurlsArray.forEach(({ name, email, title, body, id }) => {
                new Card(name, email, title, body, id).render(".container")

            })


        })
        .catch(error => {
            console.error("This is error " + error.message);
        })
}

getUsersCards();


const divCarddeleteOnServer = ((cardId, divCard) => {
    fetch(`${geturl}/${cardId}`, {
        method: `DELETE`
    })

        .then((response) => {
            if (response.status === 200) {
                console.log(response)
                divCard.remove();
            }
        })
        .catch(error => {
            console.error("This is error:   " + error.message)
        })
})


