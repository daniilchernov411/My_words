'use strict'

let thisInterval;
let timerTime;
let downloadTimer;
let showImages = ["./img/2.jpg", "./img/3.jpg", "./img/4.png", "./img/1.jpg"], a = -1;

function time () {
    let time_go = 3;
    downloadTimer = setInterval(function () {
        document.getElementById("timer").innerText = time_go;
        time_go -= 1;
        if (time_go <= 0) 
            clearInterval(downloadTimer);
    }, 1000);
}

function displayShowImages() {
    a = (a === showImages.length - 1) ? 0 : a + 1;
    document.getElementById("img").src = showImages[a];
}

thisInterval = setInterval(displayShowImages, 10000);
timerTime = setInterval(time, 10000);

document.getElementById("stop").addEventListener('click', function () {
    clearInterval(thisInterval);
    clearInterval(timerTime);
    clearInterval(downloadTimer);
});

document.getElementById("resume").addEventListener('click', function () {
    thisInterval = setInterval(displayShowImages, 10000);
    timerTime = setInterval(time, 10000);
});


















