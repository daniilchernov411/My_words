// Теоретичні питання
//1) Опишіть, як можна створити новий HTML тег на сторінці.
//2) Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
//3) Як можна видалити елемент зі сторінки?

// 1) Створення елемента JavaScript виконується за допомогою методу createElement:
// const $elem = document.createElement('tag');
// Замість tag необхідно вказати тег елемента, який потрібно створити.
// Наприклад, створимо елемент p:
// const $elem = document.createElement('p');
// Створення текстового вузла JavaScript здійснюється за допомогою методу createTextNode:
// const text = document.createTextNode('text');
// В аргументі createTextNode необхідно розмістити текст, який повинен мати цей текстовий вузол.
// Наприклад, створимо текстовий вузол із текстом «Я новий текстовий вузол»:
// const text = document.createTextNode('Я новий текстовий вузол');
// 2) Перший параметр – це кодове слово, яке вказує, куди вставити відносно elem. Має бути одним із наступних:
// "beforebegin"- вставити htmlбезпосередньо перед elem,
// "afterbegin" - вставити htmlв elem, на початку,
// "beforeend" - вставте htmlв elem, в кінці,
// "afterend" - вставити html відразу після elem.
// 3) Для видалення елемента є методи node.remove().

'use strict';

function showList(list) {

    function showUl(show, Ul) {
        let ul = document.createElement('ul');
        let li;

        show.appendChild(ul);

        Ul.forEach(function (item) {
            if (Array.isArray(item)) {
                showUl(li, item);
                return;
            }

            li = document.createElement('li');
            li.appendChild(document.createTextNode(item));
            ul.appendChild(li);
        });
    }

    let div = document.getElementById('thisList');

    showUl(div, list);
// }
    let current = 3;
    let timerId = setInterval(function () {
        document.getElementById("timer").innerText = current;
        if (current === 0) {
            document.getElementById('thisList').classList.add("none");
            document.getElementById('timer').classList.add("none");
            clearInterval(timerId);
        }
        current--;
    }, 1000);

}

showList(['hello', 'world', ["Dima", "Bogutskii"], 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']);
//працює лише для списків у масиві;




















































