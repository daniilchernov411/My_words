const listFilm = document.querySelector('.list');
const url = "https://ajax.test-danit.com/api/swapi/films";

fetch(url)
    .then((response) => response.json())
    .then((item) =>
        item.forEach((film) => {
            const text = `<li class='movie' data-id='${film.id}'>назва фільму: ${film.name}<p>номер епізоду: ${film.episodeId}</p><p>короткий зміст: ${film.openingCrawl}</p><p>персонажи: </p></li>`;

            listFilm.insertAdjacentHTML("beforeend", text);
            const characters = [];
            const description = [];

            film.characters.forEach((character) => {
                description.push(
                    fetch(character)
                        .then((response) => response.json())
                        .then((people) => {
                            characters.push(people.name);
                        })
                );
            });
            Promise.all(description).then(() => {
                let character = "";
                characters.forEach((people) => {
                    character += `<span>${people + ", "}</span>`;
                });

                const filmItem = document.querySelector(`.movie[data-id='${film.id}']`);
                console.log(filmItem);
                filmItem.insertAdjacentHTML("beforeend", character);
            });
        })
    )




