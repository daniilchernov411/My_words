import gulp from 'gulp';
import imagemin from 'gulp-imagemin';
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
import clean from 'gulp-clean';
import autoprefixer from 'gulp-autoprefixer';
import cleanCss from 'gulp-clean-css';
import rename from 'gulp-rename';
import browserSync from 'browser-sync';
import uglifyJs from 'gulp-uglify';
import concat from 'gulp-concat';

const BS = browserSync.create();
const sass = gulpSass(dartSass);

async function minifyImg() {
	gulp.src('./src/img/**/*')
		.pipe(imagemin())
		.pipe(gulp.dest('./dist/img'))
};

const buildStyles = () => gulp.src('./src/scss/index.scss')
	.pipe(sass.sync().on('error', sass.logError))
	.pipe(autoprefixer({
		cascade: false,
		overrideBrowserslist: ['> 0.1%']
	}))
	.pipe(cleanCss({ compatibility: 'ie8' }))
	.pipe(rename('styles.min.css'))
	.pipe(gulp.dest('./dist/css'));

const buildJs = () => gulp.src('./src/js/**/*.js')
	.pipe(concat('scripts.min.js'))
	.pipe(uglifyJs())
	.pipe(gulp.dest('./dist/js/'));


function cleanDist(cb) {
		return gulp.src('./dist')
		.pipe(clean())
		cb();
};

const cleanDistImg = () => gulp.src('./dist/img')
	.pipe(clean());

function browsersyncServe(cb) {
	browserSync.init({
		server: {
			baseDir: '.'
		}
	});
	cb();
};

function browsersyncReload(cb) {
	browserSync.reload();
	cb();
};

function watchTask() {
	gulp.watch('*.html', browsersyncReload);
	gulp.watch('./src/img/**/*', gulp.series(cleanDistImg, minifyImg, browsersyncReload));
	gulp.watch('./src/scss/**/*.scss', gulp.series(buildStyles, browsersyncReload));
	gulp.watch('./src/js/**/*.js', gulp.series(buildJs, browsersyncReload));
};

export const build = gulp.series(cleanDist, gulp.parallel(buildStyles, buildJs, minifyImg));
export const dev = gulp.parallel(browsersyncServe, watchTask);
export const minifyImgRun = minifyImg;
export const buildStylesRun = buildStyles;
export const buildJsRun = buildJs;
export const cleanDistRun = cleanDist;
export const browsersyncServeRun = browsersyncServe;