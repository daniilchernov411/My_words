// Теоретичні питання
// 1) Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
// 2) Які засоби оголошення функцій ви знаєте?
// 3) Що таке hoisting, як він працює для змінних та функцій?

// 1) Екранування -  спеціальні символи, які мають особливе значення у регулярному вираженні, як  у звичайних рядках. У регулярних висловлюваннях багато буває екранування, так багато спецсимволів бере участь, а ці символи повинні бути наприклад у тексті (у телефоні дужки та дефіси), хоча самі по собі дужки та дефіси можуть бути конструкціями цих же виразів.

// 2) Оголошення функцій: 
// 1. Функції типу "function declaration statement": 
// Назва функції.
// Список параметрів  укладених у круглі дужки ()та розділених комами.
// Інструкції, які будуть виконані після виклику функції, укладають фігурні дужки { }.
// 2. Функції типу "function definition expression": така функція може бути  анонімною.

// 3) Hoisting -  це механізм, в якому змінні та оголошення функцій, пересуваються вгору своєї області видимості перед тим, як код буде виконано.
// Розміщення оголошення функцій на згадку, перед виконанням будь-якого сегмента коду. Оголошення функцій піднімаються, але вони йдуть на верх, тому вони будуть знаходитися над усіма оголошеннями змінних. Це дозволяє використовувати функцію до того, як ми оголосимо її у своєму коді.
// Використовувати змінну в коді до її оголошення та/або ініціалізації. Однак піднімає лише оголошення, а не ініціалізації. Це означає, що ініціалізація не відбудеться, доки не буде виконано пов’язаний рядок коду, а потім оголошена, або оголошена та ініціалізована в одному рядку.



'use strict';

function createNewUser(){
    let newUser = {
        getAge: function () {
            let now = new Date();
            let currentYear = now.getFullYear();

            let inputDate = +this.birthday.substring(0, 2);
            let inputMonth = +this.birthday.substring(3, 5);
            let inputYear = +this.birthday.substring(6, 10);

            let birthDate = new Date(inputYear, inputMonth - 1, inputDate);
            let birthYear = birthDate.getFullYear();
            let age = currentYear - birthYear;
            if (now < new Date(birthDate.setFullYear(currentYear))) {
                age = age - 1;
            }
            return console.log(age);
        },

        getPassword: function () {

            return console.log(this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.substring(6, 10));
        }
    };

    newUser.firstName = prompt("Write your first name");
    newUser.lastName = prompt("Write your last name");
    newUser.birthday = prompt("Write your birthday DD.MM.YY: ");
    newUser.getAge();
    newUser.getPassword();

    return newUser;
}

createNewUser();