// Теоретичні питання
// 1) Описати своїми словами у кілька рядків, навіщо у програмуванні потрібні цикли.
// 2) Опишіть у яких ситуаціях ви використовували той чи інший цикл в JS.
// 3) Що таке явне та неявне приведення (перетворення) типів даних у JS?

// 1) Для решения однотипных задач. Циклы позволяют выполнять один и тот же кусок кода много раз. 
// 2) Цикл for - используется когда известно точное количество повторений. 
// Основные части конструкции цикла «for»:
// инициализация; условие; финальное выражение; тело цикла.
// Цикл while - предназначен для многократного выполнения одних и тех же инструкций до тех пор, пока истинно некоторое условие. 
// Цикл do...while -  также как и цикл «while», выполняет одни и те же инструкции до тех пор, пока указанное условие истинно. Но в отличие от while в do...while условие проверяется после выполнения инструкций. 
// 3) Явне перетворення - це перетворення типів, яке явно визначено в програмі. Він визначається користувачем у програмі.
// Неявне перетворення - це автоматичне перетворення за допомогою компілятор виконує самостійно без втручання програміста.

let list = [];

let inputNumber = +prompt("Choose a number from 0 to ...");
for (let i = 1; i <= inputNumber; i++) {
    if (i % 5 === 0) {
        list.push(i);
    }
}
if (list.length === 0) {
    alert("Sorry, no numbers")
} else {
    alert(list);
}

