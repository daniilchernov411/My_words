// Теоретичні питання
// 1) Опишіть своїми словами що таке Document Object Model (DOM)
// 2) Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// 3) Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

// 1) DOM - це модель документа, яку браузер створює в пам'яті комп'ютера на підставі коду HTML, отриманого ним від сервера.
// 2) innerText витягує та встановлює вміст тега у вигляді простого тексту. Показує весь текстовий вміст, який не відноситься до синтаксису HTML.
// innerHTML витягує та встановлює вміст у форматі HTML. Покаже текстову інформацію по одному елементу.
// 3) Якщо елемент має атрибут id, ми можемо отримати його за допомогою методу document.getElementById(id).
// Найуніверсальніший метод - elem.querySelectorAll(css) -> він повертає всі елементи всередині elem, що відповідають заданому CSS-селектору.

// function makeRed() {
//   document.querySelectorAll('p').forEach(el => el.style.backgroundColor = 'red');
// console.log(makeRed);
// }

const makeRed = document.querySelectorAll("p");
makeRed.forEach((p) => p.style = "background-color: #ff0000");
console.log(makeRed);


const elem = document.getElementById('optionsList');
console.log(elem);
console.log(elem.parentElement);
// - батьківський елемент
console.log(elem.childNodes);
// - дочірні елемент


const test = document.querySelector('#testParagraph')
test.innerText = "This is a paragraph";


// const mainheader = document.querySelector('.main-header');
// const headernew = mainheader.new;
// for (const mainnew of headernew) {
//     mainnew.classList.add('nav-item');
// }
// console.log(headernew);

const mainheader = document.querySelector('.main-header');
mainheader.classList.remove('main-header')
mainheader.classList.add('nav-item')


const section_remove = document.querySelectorAll('.section-title');
section_remove.forEach((subtitle) => subtitle.classList.remove('section-title'))
























































