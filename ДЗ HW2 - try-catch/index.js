// Теоретичне питання
// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.

// для проверки данных, введеных пользователем на странице;
// для более гибкого вывода ошибок (вывести алертом, показать разные модалки, маленькие подсказки...);
// понятным языком описать ошибку для пользователя;
// для проверки данных, которые приходят с бекенда, например в json.


const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  }
];



let divContainer = document.querySelector("#root");
let booksList = document.createElement("ul");
divContainer.append(booksList);

class BooksItem {
  constructor({ author, name, price }) {
    this.author = author;
    this.name = name;
    this.price = price;
  }

  createCards() {
    this.listItem = document.createElement("li");
    booksList.append(this.listItem);
    booksList.classList.add('list');
    this.listItem.classList.add('books-item');
    this.listItem.innerText = `${this.author};
          ${this.name};
          ${this.price}; `

  }
}

let ErrorAuthor = new Error(`Автор не отображен в №:`);
let ErrorName = new Error(`Название книги не отображено в №:`);
let ErrorPrice = new Error(`Цена не отображена в №:`);


books.forEach((elem, index) => {
  try {
    if (elem.author !== undefined && elem.name !== undefined && elem.price !== undefined) {
      new BooksItem(elem).createCards();
    }

    else if (elem.author === undefined) {
      throw ErrorAuthor;
    }

    else if (elem.name === undefined) {
      throw ErrorName;
    }

    else if (elem.price === undefined) {
      throw ErrorPrice;
    }

  } catch (err) {
    console.error(err + `${index + 1}`
    );
  }
});
